import React from 'react';
import Header from './components/header';
import NavBar from './components/navbar';
import ThumbnailBar from './components/thumbnail';
import ReadingPane from './components/readingpane';

import styles from './styles.scss';
import { LABELS_LOADED, LABELS_DETAILS_LOADED,
        THREADS_LOADED, THREADS_REQUESTED, THREADS_RESET, NAV_SELECT } from './state/events';

import Base64 from './base64.js';

window.Base64 = Base64;


export default class App extends React.Component {

    constructor(props) {
        super(props);
        this.props = props;
        const { store, apiEndpoints } = props;

        this.store = store;
        this.apiEndpoints = apiEndpoints;

        store.subscribe(() => this.render());

        this.loadLabels();
    }

    loadLabels = () => {
        const { labels } = this.apiEndpoints;
        const store = this.store;

        labels.get().then((data) => {
            store.dispatch({type: LABELS_LOADED, data: data.labels});

            labels.getDetails(data.labels).then((entries) => {
                const dict_entries = entries.reduce((memo, each) => {
                    memo[each.id] = each;
                    return memo;
                }, {});
                store.dispatch({type: LABELS_DETAILS_LOADED, data: dict_entries});
            });
        });
    }


    loadThreads = () => {
        const { threads } = this.apiEndpoints;
        const store = this.store;

        const state = store.getState();

        store.dispatch({type: THREADS_REQUESTED});
        threads.get([state.selectedFolder], state.threads.length > 0).then((data) => {
            store.dispatch({type: THREADS_LOADED, data: [...state.threads, ...data]});
        });
    }

    onLabelSelect = (labelId) => {
        const state = this.store.getState();

        if (state.selectedFolder !== labelId) {
            this.store.dispatch({type: THREADS_RESET});
            this.store.dispatch({type: NAV_SELECT, data: labelId});
        }

        this.loadThreads();
    }

    render() {
        return (
            <div className={styles.container}>
                <div className={styles.header}>
                    <Header />
                </div>
                <div className={styles.navbar}>
                    <NavBar onLabelSelect={(lid) => this.onLabelSelect(lid)} />
                </div>
                <div className={styles.thumbnailBar}>
                    <ThumbnailBar containerHeight={window.screen.height - 120}
                        cursor={this.loadThreads}/ >
                </div>

                <div className={styles.readingPane}>
                    <ReadingPane />
                </div>
            </div>
        );
    }
}
