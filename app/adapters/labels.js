

const folders = {'IMPORTANT': {title: 'Important'},
                'SENT': {title: 'Sent'},
                'DRAFT': {title: 'Drafts'},
                'TRASH': {title: 'Trash'}};

export default class LabelReader {

    constructor(labels) {
        this.labels = labels;
        this._l_dict = labels.reduce((memo, e) => {
            memo[e.id] = e;
            return memo;
        }, {});

        this.categories = {
            [LabelReader.CATEGORY.SYSTEM]: this._getSystem(),
            [LabelReader.CATEGORY.USER]: this._getUsers(),
            [LabelReader.CATEGORY.FOLDERS]: this._getFolders()
        }
    }

    _getFolders = () => {
        const result = Object.keys(folders).map((key) => {
            const label = this._l_dict[key];
            if (label) {
                return Object.assign({title: folders[key].title}, label);
            }
            return null;
        }).filter((each) => each !== null);
        return result;
    }

    _getUsers = () => {
        return this.labels.filter((each) => {
            if(each.type === LabelReader.CATEGORY.USER) {
                each.title = each.name;
                return true;
            }
        });
    }

    _getSystem = () => {
        return this.labels.filter((each) => {
            return (each.type === LabelReader.CATEGORY.SYSTEM);
        });
    }

    get = (category) => {
        return this.categories[category];
    }

    findByIds = (category, [first, ...rest]) => {
        return this._l_dict[first];
    }
}
LabelReader.CATEGORY = { SYSTEM: 'system', USER: 'user', FOLDERS: 'folders'};


