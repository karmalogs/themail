import Base64 from '../base64';
import moment from 'moment';

const HEADER_KEYS = ['To', 'Subject', 'From', 'Date'];

class MessageReader {

    getmeta(headers) {
        const meta_dict = HEADER_KEYS.reduce((memo, k) => {
            memo[k] = null;
            return memo;
        }, {});

        for(let header of headers) {
            if (meta_dict[header.name] !== undefined) {
                meta_dict[header.name] = header.value.replace(/"/, '');
            }
        }

        return meta_dict;
    }


    overview = (message) => {
        const labels = [...message.labelIds].reverse();

        return {
            id: message.threadId,
            labelIds: message.labelIds,
            snippet: message.snippet,
            shortDate: moment(message.internalDate, 'x').format('DD MMM'),
            read: labels.indexOf('UNREAD') === -1,
            labels,
            ...this.getmeta(message.payload.headers)
        };
    }

    extract = (message) => {
        const meta = this.getmeta(message.payload.headers);

        return {
            id: message.id,
            labelIds: message.labelIds,
            payload: Base64.decode(message.payload.body.data || ''),
            threadId: message.threadId,
            snippet: message.snippet,
            date: moment(message.internalDate, 'x').format('DD MMM'),
            meta: this._getmeta(message.payload.headers)
        }
    }
}


export default class ThreadReader {

    constructor(threads) {
        this.threads = threads;
    }


    getOverview = () => {
        return this.threads.map((thread) => {
            const [latest, ...rest] = [...thread.messages].reverse();
            const reader = new MessageReader();

            const overview = reader.overview(latest);

            return {
                id: thread.id,
                count: thread.messages.length,
                ...overview
            }
        });
    }

    resolve = () => {
        return this.threads.map((thread) => {
            const result = thread.result;
            const messageReader = new MessageReader();

            const messages = thread.messages.reverse().map((message) => {
                return messageReader.extract(message);
            });

            return {
                'id': thread.id,
                'count': thread.messages.length,
                messages,
                thread
            }
        });
    }
}
