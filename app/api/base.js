
export class BaseApi {

    constructor(api) {
        this.api = api;
    }

    execute(apiPromise) {
        return new Promise((resolve, reject) => {
            apiPromise.execute((response) => {
                if (response.error) {
                    reject(response.message);
                } else {
                    resolve(response);
                }
            });
        });
    }

    executeBatch(apiPromises) {

        if (apiPromises.length > 0) {
            const batch = apiPromises.reduce((memo, promise) => {
                memo.add(promise.request, {id: promise.id});
                return memo;
            }, this.api.client.newBatch());

            const successHandler = (response, resolve, reject) => {
                let hasError = false;
                const resultSet = response.result;

                for(let k of Object.keys(resultSet)) {
                    const response = resultSet[k];
                    if (response.status !== 200) {
                        hasError = true;
                    }
                }

                if (!hasError) {
                    resolve(resultSet);
                } else {
                    reject(response);
                }

            },

            errorHandler = (response, resolve, reject) => {
                console.log(response);
                reject(response);
            };

            return new Promise((resolve, reject) => {
                batch.then((response) => successHandler(response, resolve, reject),
                           (response) => errorHandler(response, resolve, reject));
            });
        }
        return new Promise((resolve, reject) => {
            resolve([]);
        });
    }
}

export class PaginatableApi extends BaseApi {

    constructor(api) {
        super(api);
        this.cursor_token = null;
    }

    executePage(apiPromise) {
        const promise = super.execute(apiPromise);
        return new Promise((resolve, reject) => {
            promise.then((response) => {
                this.cursor_token = response.result.nextPageToken;
                resolve(response);
            }).catch(reject);
        });
    }

    get_cursor() {
        return this.cursor_token;
    }

    next = () => {
        throw new Error('Method not implemented');
    };
}


