import ThreadsApi from './threads';
import LabelsApi from './labels';


export default class MailApi {

    constructor(gapi) {
        this.api = gapi;
        this.init = this._init();
    }

    _init() {
        const api = this.api;
        return new Promise((resolve, reject) => {
            api.client.load('gmail', 'v1', () => {
                resolve({
                    threads: new ThreadsApi(api),
                    labels: new LabelsApi(api)
                });
            });
        });
    }

}
