import { BaseApi } from './base';


const _colors = ['#6672B5', '#45020F', '#67A442', '#A3584A', '#E53950', '#FC76F2', '#C37D67',
                    '#CCF713', '#9C6F4A', '#920DB6', '#DA2411', '#6E5EBB', '#6875F9']
export default class LabelsApi extends BaseApi {

    get = () => {
        return this.execute(this.api.client.gmail.users.labels.list({
                userId: 'me'
            })
        );
    }

    getDetails = (labels) => {
        return new Promise((resolve, reject) => {
            const promises = labels.map((each) => {
                return {
                    request: this.api.client.gmail.users.labels.get({
                                userId: 'me',
                                id: each.id
                            }),
                    id: each.id
                }
            });

            this.executeBatch(promises).then((responses) => {
                const labelsWithColors = promises.map((each, i) => {
                    const label = responses[each.id].result;

                    if (label.type === 'user') {
                        label.color = _colors[i];
                    }
                    return label;
                });

                resolve(labelsWithColors);
            }).catch(reject);
        });
    };
}
