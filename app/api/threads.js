import { PaginatableApi } from './base';


export default class ThreadsApi extends PaginatableApi {

    _details(threadId) {
        return this.api.client.gmail.users.threads.get({
            userId: 'me',
            id: threadId,
            format: 'metadata'
        });
    }

    get = (labelIds, next = false) => {
        const options = {userId: 'me', maxResults: 30, labelIds};

        if (next === true) {
            options['pageToken'] = this.get_cursor()
        }

        return new Promise((resolve, reject) => {
            this.executePage(this.api.client.gmail.users.threads.list(options))
                .then((response) => {
                    const threads = response.result.threads;
                    const promises = (threads || []).map((thread) => {
                        return {request: this._details(thread.id),
                                id: thread.id};
                    });
                    this.executeBatch(promises).then((responses) => {
                        const sorted = promises.map((promise) => {
                            return responses[promise.id].result;
                        });
                        resolve(sorted);
                    }).catch(reject);
                });
        });
    }

    next = () => this.get(true)
}


