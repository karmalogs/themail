import { CLIENT_ID, SCOPES } from './config';

export default class LoginHandler {
    constructor(gapi) {
        this.api = gapi
    }

    authorize = () => {
        return new Promise((resolve, reject) => {
            this.api.auth.authorize({

                client_id: CLIENT_ID,
                scope: SCOPES.join(','),
                immediate: false

            }, (result) => {
                if (!result) {
                    reject('No response from server');
                } else if (result.error) {
                    reject(result.error);
                } else {
                    resolve(gapi);
                }
            });
        });
    }
}
