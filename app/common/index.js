import React from 'react';
import classNames from 'classnames';
import styles from './styles.scss';


const Icons = {
    get: function(cls, ...classes) {
        const conditionals = { [styles.tmicon]: cls.startsWith('icon-')};
        return classNames(conditionals, styles[cls], ...classes);
    }
};


export const ColoredIcon = ({ color, className }) => {
    return (
        <span className={classNames(styles.label, className)} style={{backgroundColor: color}} ></span>
    );
};

export const FontIcon = ({ icon }) => {
    return (
        <span className={Icons.get(icon)}></span>
    );
};

