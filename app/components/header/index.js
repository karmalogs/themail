import React from 'react';
import styles from './style.scss';
import classNames from 'classnames';
import { FontIcon } from '../../common';


class Compose extends React.Component {

    render() {
        return (
            <button className={styles.compose_button}>COMPOSE</button>
        );
    }
}


class SearchBox extends React.Component {

    render() {
        return (
            <input type="text" className={styles.search_input}></input>
        )
    }
}

class Menu extends React.Component {
    render() {
        return (
            <ul className={styles.menu_list}>
                <li className={styles.menu_list_item}>
                    <FontIcon icon='icon-switch' />
                </li>
            </ul>
        ) 
    }
}

export default class Header extends React.Component {

    render() {
        return (
            <div className={styles.header} >
                <div className={styles.section + ' f_min'}>
                    <Compose />
                </div>
                <div className={styles.section + ' f_max'}>
                    <SearchBox />
                </div>
                <div className={styles.section + ' f_min'}>
                    <Menu />
                </div>
            </div>
        );
    }
}
