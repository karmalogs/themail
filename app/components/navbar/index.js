import React from 'react';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';
import styles from './styles.scss';
import moment from 'moment';
import classNames from 'classnames';
import { FontIcon, ColoredIcon } from '../../common';
import { LabelReader } from '../../adapters';


class MenuItem extends React.Component {
    constructor(props) {
        super(props);
    }

    onClick() {
        this.props.onClick(this.props.id);
    }

    render() {
        const className = classNames({
            [styles.section_menuitem]: true,
            [styles.menuitem__selected]: this.props.selected
        });

        return (
            <div className={className} onClick={this.onClick.bind(this)}>
                <div className={styles.section_menuitem__icon}>
                    {this.props.icon}
                </div>
                <div className={styles.section_menuitem__label}>
                    {this.props.label}
                </div>
                <div className={styles.section_menuitem__meta}>
                    <span className={styles.section_menuitem__meta__unread}>{this.props.meta > 0? this.props.meta: ''}</span>
                </div>
            </div>
        );
    }
}

MenuItem.defaultProps = {
    selected: false
};


class Inbox extends React.Component {

    shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState);
    }

    render() {
        return (
            <div className={styles.section_inbox}>
                <MenuItem label='Inbox' id='inbox' selected={this.props.selected}
                    icon={<FontIcon icon='icon-drawer2' />}
                    onClick={this.props.onClick}
                    meta={this.props.unread} />
            </div>
        );
    }
}

class Folders extends React.Component {

    shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState);
    }

    render() {
        return (
            <div className={styles.section_folders}>
            {
                this.props.folders.map((folder) => {
                    return <MenuItem key={folder.id} id={folder.id} selected={this.props.selectedKey === folder.id}
                                onClick={this.props.onClick}
                                label={folder.title} meta={folder.unread} />
                })
            }
            </div>
        );
    }
}


class Favorites extends React.Component {

    shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState);
    }

    render() {
        return (
            <div className={styles.section_favorites}>
                <MenuItem label='Favorites' id='favorites' selected={this.props.selected}
                icon={<FontIcon icon='icon-heart'/>}
                onClick={this.props.onClick}
                meta={this.props.unread} />
            </div>
        );
    }
}


class Labels extends React.Component {

    shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState);
    }

    render() {
        return (
            <div className={styles.section_labels}>
                {
                    this.props.labels.map((label) => {
                        return <MenuItem key={label.id} id={label.id} 
                                icon={<ColoredIcon color={label.color} />}
                                label={label.title} selected={this.props.selectedKey === label.id}
                                onClick={this.props.onClick}
                                color={label.color}
                                meta={label.unread} />
                    })
                }
            </div>
        );
    }
}


const NavBar = ({ inbox, favorites, folders, labels, selectedKey,
                    onInboxSelect, onFavoritesSelect, onFolderSelect, onLabelSelect }) => {

    const sections = [
        <Inbox unread={inbox.unread} selected={selectedKey === 'INBOX'} onClick={onInboxSelect} />,
        <Folders folders={folders} selectedKey={selectedKey} onClick={onFolderSelect} />,
        <Favorites unread={favorites.unread}  selected={selectedKey === 'STARRED'}
            onClick={onFavoritesSelect} />,
        <Labels labels={labels} selectedKey={selectedKey} onClick={onLabelSelect} />
    ];

    return (
        <div className={styles.navbar}>
            {sections.map((each, i) => {
                    return (
                        <div className={classNames(styles.section)} key={i}>
                            { each }
                        </div>
                    )
                })
            }

        </div>
    );
};


const mapStateToProps = (state, defprops) => {

    const reader = new LabelReader(state.labels);
    const selectedKey = state.selectedFolder;

    return {inbox: state.inbox,
            favorites: state.favorites,
            folders: reader.get(LabelReader.CATEGORY.FOLDERS),
            labels: reader.get(LabelReader.CATEGORY.USER),
            selectedKey };

};

const mapDispatchToProps = (dispatch, props) => {

    const onInboxSelect = () => {
        props.onLabelSelect('INBOX');
    },

    onFavoritesSelect = () => {
        props.onLabelSelect('STARRED');
    },

    onFolderSelect = props.onLabelSelect,

    onLabelSelect = props.onLabelSelect;

    return { onInboxSelect, onFavoritesSelect, onFolderSelect, onLabelSelect };
};

export default connect(mapStateToProps, mapDispatchToProps)(NavBar);

