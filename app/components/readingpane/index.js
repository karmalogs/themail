import React from 'react';
import styles from './styles.scss';
import classNames from 'classnames';
import Infinite from 'react-infinite';


class Section extends React.Component {
    render() {
        return (
            <div className={styles.message_container}>
                <div className={styles.summary}>
                    <div className={styles.avatar}>
                        <img className={styles.avatar_img} src={this.props.sender.avatar_url} />
                    </div>
                    <div className={styles.details}>
                        <div className={styles.row}>
                            <div className={styles.sender_name}>
                                {this.props.sender.name}
                            </div>

                            <div className={styles.label}>
                                <span className={classNames({ [styles.label_icon]: true, [this.props.label]: true})}></span>
                                <span className={styles.label_title}>Recruitment</span>
                            </div>
                        </div>
                        <div className={styles.row}>
                            <div className={styles.sender_email}>
                                {this.props.sender.email_address}
                            </div>
                            <div className={styles.meta}>
                                {this.props.date}
                            </div>
                        </div>
                    </div>
                </div>
                <div className={styles.message}>
                Hi Kailash,<br/>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.

It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
<br/>
Regards
Kailash K
                </div>
            </div>
        );
    }
}

export default class ReadingPane extends React.Component {

    render() {
        const props = {
            sender: {
                id: 100,
                avatar_url: "https://lh5.googleusercontent.com/-xt5snEoygZc/AAAAAAAAAAI/AAAAAAAAAB0/mg-PYIgLbh0/photo.jpg?sz=50",
                name: 'Kailash',
                email_address: 'retadedgeek@gmail.com'
            },
            date: '29 Mar'
        };
        return (
            <div className={styles.container}>
                <Section label='pink' {...props} />
            </div>
        );
    }
}
