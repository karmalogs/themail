import React from 'react';
import { connect } from 'react-redux';
import styles from './styles.scss';
import classNames from 'classnames';
import Infinite from 'react-infinite';
import { ThreadReader, LabelReader } from '../../adapters';
import { THREAD_SELECTED } from '../../state/events';
import { ColoredIcon, FontIcon } from '../../common';


const Card = ({read, selected, from, dateTime, subject, snippet, label, stackSize, onClick}) => {
    return (
        <div className={classNames({[styles.card]: true,
                                    [styles.read]: read === true,
                                    [styles.selected]: selected})}
            onClick={onClick} >

            <div className={styles.selector}>
                <input type="checkbox"></input>
            </div>

            <div className={styles.content}>
                <div className={styles.meta}>
                    <div className={styles.meta_row}>
                        <div className={styles.meta_sender}>
                            {from}
                        </div>
                        <div className={styles.meta_date}>
                            <span className={styles.meta_date_day}>{dateTime} </span>
                        </div>
                    </div>
                    <div className={styles.meta_row}>
                        <span className={styles.meta_subject}>
                            {subject}
                        </span>
                    </div>

                </div>
                <div className={styles.message}>
                    {snippet}
                </div>
            </div>

            <div className={styles.summary}>
                {label.color? <ColoredIcon color={label.color} className={{'hide': !label}} />: ''}
                {stackSize > 1? <span className={styles.stack_size}>{stackSize} </span>: ''}
            </div>
        </div>
    )
};

Card.propTypes = {
    read: React.PropTypes.bool.isRequired,
    selected: React.PropTypes.bool.isRequired,
    onClick: React.PropTypes.func.isRequired,

    from: React.PropTypes.string.isRequired,
    dateTime: React.PropTypes.string.isRequired,
    subject: React.PropTypes.string.isRequired,
    snippet: React.PropTypes.string.isRequired,
    stackSize: React.PropTypes.number.isRequired,
    label: React.PropTypes.string
};


const ThumbnailBar = ({ loading, loader, page, entries, containerHeight, selectedThreadId, elementHeight = 120,
                        onFetchMore, onThreadSelect }) => {

    const elements = entries.map((props) => {
        return <Card read={props.read}  from={props.From} dateTime={props.shortDate}
                subject={props.Subject} snippet={props.snippet} 
                selected={props.id === selectedThreadId} label={props.label} stackSize={props.count}
                onClick={() => onThreadSelect(props.id)}/>
    });


    const containerClassNames = classNames(styles.container, {[styles.loading]: loading});

    if (entries.length > 0 && elementHeight * entries.length < containerHeight) {
        onFetchMore = () => {};
        loader = null;
    }
    let loadOffset = containerHeight - elementHeight;

    return (
            <Infinite
                className={containerClassNames}
                elementHeight={elementHeight}
                infiniteLoadBeginEdgeOffset={loadOffset}
                containerHeight={containerHeight - elementHeight}
                onInfiniteLoad={onFetchMore}
                loadingSpinnerDelegate={loader}
                isInfiniteLoading={loading}
                >
                {elements}
            </Infinite>
        );

};

ThumbnailBar.propTypes = {
    containerHeight: React.PropTypes.number.isRequired,
    cursor: React.PropTypes.func.isRequired
};


const mapStateToProps = (state, defprops) => {

    const threads = state.threads;
    const reader = new ThreadReader(threads);

    const labelReader = new LabelReader(state.labels);

    const loader = (
        <div className={styles.infinite_loader}>
            Loading...
        </div>
    );

    const entries = reader.getOverview().map((entry) => {
        const primary = labelReader.findByIds(LabelReader.CATEGORY.USER, entry.labels);
        entry.label = primary;
        return entry;
    });

    return {
        loading: state.threadsLoading,
        loader: loader,
        page: state.threadPage,
        selectedThreadId: state.selectedThreadId,
        entries: entries
    };
};


const mapDispatchToProps = (dispatch, props) => {
    const onFetchMore = () => {
        props.cursor();
    },

    onThreadSelect = (threadId) => {
        dispatch({type: THREAD_SELECTED, data: threadId});
    };

    return { onFetchMore, onThreadSelect };
};


export default connect(mapStateToProps, mapDispatchToProps)(ThumbnailBar);
