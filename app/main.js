import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';

import App from './App.js';
import LoginHandler from './auth/login';
import MailApi from './api';
import gapi from 'gapi';
import reducers from './state/reducers';


const doLogin = () => {
    return new Promise((resolve, reject) => {
        gapi.load('client', {
            'callback': () => {
                new LoginHandler(gapi).authorize().then(resolve).catch(reject);
            }
        });
    });
};


const store = createStore(reducers);


const render_app = (endpoints) => {
    ReactDOM.render(<Provider store={store}>
        <App store={store} apiEndpoints={endpoints} />
    </Provider>, document.getElementById('root'));
};


doLogin().then((api) => {
    const m_api = new MailApi(api);
    m_api.init.then((endpoints) => {
        render_app(endpoints);
    });

})
.catch((error) => {
    console.log(error);
});

