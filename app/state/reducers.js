import { combineReducers } from 'redux';
import { LABELS_LOADED, LABELS_DETAILS_LOADED, NAV_SELECT,
        THREADS_LOADED, THREADS_REQUESTED, THREADS_RESET,
        THREAD_SELECTED } from './events';


const inbox = (state = {
}, action) => {

    switch(action.type) {
        case LABELS_DETAILS_LOADED:
            const unread = action.data.INBOX.threadsUnread;
            return Object.assign(state, { unread });
        default:
            return state;
    }
};


const favorites = (state = {
}, action) => {

    switch(action.type) {
        case LABELS_DETAILS_LOADED:
            const unread = action.data.STARRED.threadsUnread;
            return Object.assign(state, { unread });

        default:
            return state;
    }
};


const folders = (state = [], action) => {
    return state;
};


const labels = (state = [], action) => {

    switch (action.type) {

        case LABELS_LOADED:
            return action.data;

        case LABELS_DETAILS_LOADED:
            const data = action.data;

            return state.map((label) => {
                return Object.assign(label, {unread: data[label.id].threadsUnread,
                                            color: data[label.id].color });
            });
        default:
            return state;
    }
};


const selectedFolder = (state = 'INBOX', action) => {

    if (action.type === NAV_SELECT) {
        return action.data;
    }

    return state;
};


const threads = (state = [], action) => {

    switch(action.type) {
        case THREADS_LOADED:
            return action.data;
        case THREADS_RESET:
            return [];
        default:
            return state;
    }
};


const selectedThreadId = (state = null, action) => {
    switch(action.type) {
        case THREADS_LOADED:
            if (state === null && action.data.length > 0) {
                const [first, ...rest] = action.data;
                return first.id;
            }
            return state;
        case THREAD_SELECTED:
            return action.data;
        case THREADS_RESET:
            return null;
        default:
            return state;
    }
};


const threadsLoading = (state = false, action) => {
    switch(action.type) {
        case THREADS_REQUESTED:
            return true;
        case THREADS_LOADED:
            return false;
        default:
            return state
    }
};

export default combineReducers({
    inbox,
    favorites,
    folders,
    labels,
    selectedFolder,
    threads,
    threadsLoading,
    selectedThreadId
});

